#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>


int main() {
	int f1, f2; 
	
	char message[100]; // pour récupérer un message
	char *phrasep = "message envoyé au fils par le père";

	f1 = open("f2p", O_RDONLY);
	f2 = open("p2f", O_WRONLY);
		
	if (write(f2, phrasep, strlen(phrasep)+1) != -1) {
			sleep(2);
		
			if (read(f1, message, 100) >= 0) {
				printf("J'ai lu : %s\n", message);
			} else {
				perror("Read erreur"); exit(4);
			}
		} else {
			perror("Write erreur"); exit(3);
		}

		
		close(f1);
		close(f2);

return 0;
}