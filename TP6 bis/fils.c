#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>


int main() {
	int f1, f2; 
	
	char message[100]; // pour récupérer un message
	char *phrasef = "message envoyé au père par le fils";

	f1 = open("f2p", O_WRONLY);
	f2 = open("p2f", O_RDONLY);
		
	if (write(f1, phrasef, strlen(phrasef)+1) != -1) {
			sleep(4);
		
			if (read(f2, message, 100) >= 0) {
				printf("J'ai lu : %s\n", message);
			} else {
				perror("Read erreur"); exit(4);
			}
		} else {
			perror("Write erreur"); exit(3);
		}

		
		close(f1);
		close(f2);

return 0;
}