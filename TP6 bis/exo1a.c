#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define R 0
#define W 1

void action (int sig) {
	printf("Je capture SIGPIPE\n");
	// quand les read se font sur des tubes sans écrivain 
}

int main() {
	int fdp[2], // père vers le fils
		fdf[2]; // fils vers père
	pid_t pid;
		
	char message[100]; // pour récupérer un message
	char *phrasef = "message envoyé au père par le fils";
	char *phrasep = "message envoyé au fils par le père";

	if ((pipe(fdf) == -1) || (pipe(fdp) == -1)) {
		perror("Pipe erreur");
		exit(1);
	}
	signal(SIGPIPE, &action);
	
	if ((pid = fork()) == -1) {
		perror("Fork erreur");
		exit(2);
	}
	
	if (pid == 0) {
		close(fdf[R]);
		close(fdp[W]);
		
		if (write(fdf[W], phrasef, strlen(phrasef)+1) != -1) {
			sleep(4);
		
			if (read(fdp[R], message, 100) >= 0) {
				printf("J'ai lu : %s\n", message);
			} else {
				perror("Read erreur"); exit(4);
			}
		} else {
			perror("Write erreur"); exit(3);
		}
		
		close(fdp[R]);
		close(fdf[W]);
		exit(0);
	}
	
	close(fdp[R]);
	close(fdf[W]);
	
	if (write(fdp[W], phrasep, strlen(phrasep)+1) != -1) {
		sleep(2);
	
		if (read(fdf[R], message, 100) >= 0) {
			printf("J'ai lu : %s\n", message);
		} else {
			perror("Read erreur"); exit(4);
		}
	} else {
		perror("Write erreur"); exit(3);
	}

	
	close(fdf[R]);
	close(fdp[W]);

	return 0;
// A compléter

}