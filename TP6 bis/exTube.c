#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

int tube[2], fcntlLecture, fcntlEcriture;
char buf[10];

struct stat statLecture, statEcriture;

void noeud (struct stat *statDesc) {
	printf("Quelques caractérisitques du noeud\n");
	if (S_ISFIFO(statDesc->st_mode)) printf("\t Type : tube\n");
	else printf("\t Type : autre que tube\n");
	printf("\t Numéro : %llu\n", statDesc-> st_ino);
	printf("\t Numéro de disque : %d\n", statDesc-> st_dev);
	printf("\t Numéro de liens physiques : %d\n", statDesc-> st_nlink);
	printf("\t Taille : %lld\n", statDesc-> st_size);
}

main() {
	if (pipe(tube) == -1) { perror("Création ratée"), exit(2); }
	if ((fcntlLecture = fcntl(tube[0], F_GETFL)) == -1) {
		perror("Fcntl lecture "); exit(2); }
	if ((fcntlEcriture = fcntl(tube[1], F_GETFL)) == -1) {
		perror("Fcntl ecriture "); exit(2); }
	printf("Attributs de %d (tube[0]) : %d\n", tube[0], fcntlLecture);	
	printf("Attributs de %d (tube[1]) : %d\n", tube[1], fcntlEcriture);	
	write(tube[1], "0123456789", 10);
	read(tube[0], buf, 5);
	printf("Chaine lue : %s\n", buf);

	fstat(tube[0], &statLecture);
	fstat(tube[1], &statEcriture);
	if (memcmp(&statLecture, &statEcriture, sizeof(struct stat)) != 0) {
		fprintf(stderr, "Noeuds en lecture et écriture différents\n");
		printf("Descripteur en lecture\n"); noeud(&statLecture);
		printf("Descripteur en ecriture\n"); noeud(&statEcriture);
	} else {
		printf("Noeud en lecture et ecriture identique\n");
		noeud(&statEcriture); }
}
