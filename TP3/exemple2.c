/* fork2b.c            */


#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	
    int status;
	int i, n = 3;
	pid_t childpid;
	
	for (i=0; i<n; i++) {
		if ((childpid = fork()) == -1) break;
		if (childpid == 0)
		printf("Processus %d avec pere %d, i = %d\n", getpid(), getppid(), i);
	}
	
	while (wait(&status) >= 0)
		;

    if (WIFEXITED(status)) {
        printf("Code de sortie du fils : %d\n", WEXITSTATUS(status));
    }
		
	return EXIT_SUCCESS;
}