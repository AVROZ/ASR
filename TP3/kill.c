#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	
    int status;
	int i, n = 5;
	pid_t pid;
	
    if ((pid = fork()) == -1) {
        printf("Problème avec le fork du processus");

        return EXIT_FAILURE;
    }

    if (pid == 0) {
        printf("Processus %d avec pere %d\n", getpid(), getppid());
        sleep(60);
    }

    if (pid > 0) {
        printf("Processus père avec id : %d\n", getpid());
        wait(&status);
    }

    printf("Valeur de status : %d", status);

    if (WIFSIGNALED(status)) {
        printf("Numéro du signal qui a causé la fin du fils : %d\n", WTERMSIG(status));
    }
		
	return EXIT_SUCCESS;
}