#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

void action_sauvegarde(int signum);

int main(int argc, char *argv[]) {
	
	signal(SIGUSR2, &action_sauvegarde);
	
	/* Charger des données */
	printf("Calcul\n");
	sleep(10);
	
	kill (getppid(), SIGUSR1);
	
	pause();

	return EXIT_SUCCESS;
}

void action_sauvegarde(int signum) {
	printf("Sauvegarde\n");
	sleep(1);
	
	kill(getppid(), SIGUSR2);
	
	pause();
}