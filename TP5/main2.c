#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>


void action1(int signum);
void action2(int signum);
void action3(int signum);

pid_t pid;

int main(int argc, char *argv[]) {
	
	printf("Mon PID de pere : %d\n", getpid());
	
	signal(SIGUSR2, &action2);
	
	pid = fork();
	
	if (pid == 0) {
		execl("travailleur2", "travailleur2", NULL);
	}
	
	signal(SIGUSR1, &action1);
	
	signal(SIGTERM, &action3);
		
	pause();
	
	return EXIT_SUCCESS;
}

void action1(int signum) {
	printf("Je tue mon fils\n");
	kill(pid, SIGKILL);
}

void action2(int signum) {
	printf("Mon fils a fini son travail\n");
	kill(pid, SIGUSR2);
	pause();
}

void action3(int signum) {
	printf("Je force mon fils à la sauvegarde\n");
	kill(pid, SIGUSR2);
	pause();
}
