#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

void action1 (int signum); /* sur SIGUSR1 */
void action2 (int signum); /* sur SIGTERM */
void action3 (int signum);/* sur SIGUSR2 */

int pid;

int main(int argc, char *argv[]) {
	
	pid = fork();
	
	if (pid == 0) {
		execl("travailleur", "travailleur", NULL);
	}
	
	printf("PID du père : %d\n", getpid());
	signal(SIGUSR1, &action1);
	signal(SIGUSR2, &action3);
	signal(SIGTERM, &action2);
	
	pause();

	return EXIT_SUCCESS;
}

void action1 (int signum) {
	/* Le fils a terminé son travail */
	printf("Mon fils m'a envoyé SIGUSR1\n");
	kill(pid, SIGUSR2);
	pause();
	kill(pid, SIGKILL);
}

void action2(int signum) {
	/* on recoit un signal de l'exterieur pour forcer la sauvegarde 
	du fils */
	printf("Le shell m'a envoyé SIGTERM\n");
	kill(pid, SIGUSR2);
	pause();
	kill(pid, SIGKILL);
}

void action3(int signum) {
	printf(" Recu SIGUSR2 du fils\n");
	kill(pid, SIGKILL);
}