#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h> 
#include <sys/wait.h>

void quithandler (int signum);
void inthandler (int signum);
void handle_sigkill (int signum);

int main(int argc, char *argv[]) {
    pid_t pid;
    sig_t s1, s2;

    s1 = signal(SIGQUIT, &quithandler);
    s2 = signal(SIGINT, &quithandler);

    if ((pid = fork()) == 0) {
        signal(SIGKILL, handle_sigkill);
        while (1) {
            printf("Je suis toujours en vie\n");
            sleep(1);
        }
    }
    else {
        sleep(5);
        printf("J'envoie SIGQUIT !\n");
        kill(0, SIGQUIT);
        sleep(5);
        printf("J'envoie SIGINT !\n");
        kill(0, SIGINT);
        sleep(5);
        printf("C'est décidé, j'envoie SIGKILL !\n");
        kill(0, SIGKILL);
        sleep(1);
    }


    return EXIT_SUCCESS;
}

void quithandler (int signum) {
    printf("Masquage du signal SIGQUIT\n");
}

void inthandler (int signum) {
    printf("Masquage du signal SIGINT\n");
}

void handle_sigkill (int signum) {
    printf("Extinction du programme\n");
    exit(SIGKILL);
}