#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

void action_sauvegarde(int signum);
int b;

int main(int argc, char *argv[]) {
	
	signal(SIGUSR2, &action_sauvegarde);
	/* charge des données */
	
	/* fait un calcul */
	b = 1;
	printf("Je fais un calcul\n"); 
	sleep (10);
	b = 0;
	kill (getppid(), SIGUSR2);
	
	pause();

	return EXIT_SUCCESS;
}

void action_sauvegarde(int signum) {
	/* sauvegarde */
	if (b==1) {
		printf("Je sauvegarde donnée corrompue\n");
		sleep(2);
	} else {
		printf("Je sauvegarde donnée ok\n");
		sleep(2);
	}
	kill(getppid(), SIGUSR1);
	pause();
}