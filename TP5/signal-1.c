#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

#define FOREVER for(;;)

static void action (int sig);

int main(int argc, char *argv[]) {
	
int i, pid, etat;
sig_t s1, s2;

s1 = signal(SIGUSR1, action);
s2 = signal(SIGUSR2, action);
if (( s1 == SIG_ERR) || (s2 == SIG_ERR)) {
	perror("Erreur attachement signal");
	exit(1);
}	

if ((pid = fork()) == 0) {
	pause();
	kill(getppid(), SIGUSR1);
	sleep(10);
}
else {
	sleep(2);
	kill(pid, SIGUSR2);
	pause();
	printf("Parent : Demande terminaison du fils\n");
	fflush(stdout);
	kill(pid, SIGTERM);
	wait(&etat);
	printf("Parent : fils terminé\n");
	fflush(stdout);
}
	return EXIT_SUCCESS;
}

static void action(int sig) {
	switch (sig) {
		case SIGUSR1 : 
			printf("Parent : signal SIGUSR1 recu\n");
			fflush(stdout);
			break;
		case SIGUSR2 : 
			printf("Fils : signal SIGUSR2 recu\n");
			fflush(stdout);
			break;
		default : 
			break;
	}
}