#include <stdio.h>
#include <stdlib.h>

int *multiplication_scal(int *matrice, int taille_matrice, int multiplicateur) {
    int i;

    for (i = 0; i < taille_matrice*taille_matrice; i++) {
        matrice[i] = matrice[i] * multiplicateur;
    }

    return matrice;
}