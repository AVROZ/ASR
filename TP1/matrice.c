#include <stdio.h>
#include <stdlib.h>
#include "calculs.h"

void affichage_matrice(int *matrice, int i, int taille, int cpt) {
    for (i = 0; i < taille*taille; i++) {
        printf("%d ", matrice[i]);

        cpt++;

        if (cpt == taille) {
            printf("\n");
            cpt = 0;
        }
    }
    
    printf("\n");
}

int main(int argc, char *argv[]) {
    int taille;
    int* matrice;
    int mult;
    int i;
    int cpt = 0;

    printf("Ecrivez la taille de la matrice : ");
	scanf("%d", &taille);

    matrice = (int*) malloc(sizeof(int) * taille * taille);

    if (matrice == NULL) {
        printf("memory not allocated.\n");
        return EXIT_FAILURE;;
    }
    
    for (i = 0; i < taille*taille; i++) {
        printf("Entrer le chiffre n°%d : ", i);
        scanf("%d", &matrice[i]);
    }

    printf("La matrice :\n");

    affichage_matrice(matrice, i, taille, cpt);

    printf("Entrer un nombre par lequel multiplier la matrice : ");
    scanf("%d", &mult);
    matrice = multiplication_scal(matrice, taille, mult);
    
    cpt = 0;
    affichage_matrice(matrice, i, taille, cpt);

    free(matrice);


    return EXIT_SUCCESS;
}