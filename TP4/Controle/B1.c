#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char* argv[]){
  pid_t pid;

  pid=fork();

  switch(pid){
    case -1:
      perror("erreur: ");
      exit(1);
    case 0:
      sleep(20);
      exit(0);
    default:
      wait(NULL);
      break;
  }
  return EXIT_SUCCESS;
}