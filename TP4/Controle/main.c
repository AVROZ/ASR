#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char* argv[]){

  pid_t pidFils1,pidFils2;

  pidFils1 = fork();

  switch(pidFils1){
    case -1:
      perror("Err: ");
      exit(1);
    case 0:
      execl("B1","B1",NULL);

    default:
      pidFils2 = fork();

      switch(pidFils2){
        case -1:
          perror("Err: ");
          exit(1);
        case 0:
          execl("B1","B1",NULL);
        default:
          break;
      }
      
      wait(NULL);
      break;
  }



  return EXIT_SUCCESS;
}