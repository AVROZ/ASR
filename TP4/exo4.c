#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char *argv[]) {
	int i = 0;
	char ch[10] = "RUN_0";
    char *buf;
	
	while (getenv(ch) != NULL) {
        sprintf(ch, "RUN_%d", i);
        printf("%s = ", ch);

        buf = getenv(ch);
        printf("%s\n", buf);

        i++;
    }
    
	return 0;
}