#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    pid_t pid;

    pid = fork();

    if (pid == 0) {
        if (execlp("ps", "ps", "-l", NULL) == -1) {
            perror("dans le exec");
            exit(0);
        }
    } else {
        wait(NULL);
    }

    return EXIT_SUCCESS;
}