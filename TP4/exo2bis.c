#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

int main(int argc, char *argv[]) {
    pid_t pid;
    char *argument[10];
    char *argument2[10];
    int valDei;
    int nb_pipe;

    if (argc <= 1) {
        perror("pas le bon nb argument");
        exit(0);
    }

    if (argc > 10) {
        perror("pas le bon nb argument");
        exit(0);
    }

    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "|")) {
            argument[i] = NULL;
            valDei = i;
            nb_pipe = 1;
        }
        argument[i] = argv[i+1];
    }
    argument[argc-1] = NULL;

    pid = fork();

    if (pid == 0) {
        if (execvp(argv[1], argument) == -1) {
            perror("dans le exec");
            exit(0);
        }
        if (execvp(argument[valDei], &argument[valDei]) == -1) {
            perror("dans le exec");
            exit(0);
        }
    } else {
        wait(NULL);
    }

    return EXIT_SUCCESS;
}