#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>

pid_t pid;

int main(int argc, char *argv[]) {
	
	pid = fork();

    if (pid != 0) {
        printf("Je suis le père\n");
        printf("Mon pid est : %d\n", (int) getpid());
        printf("Mon ppid est : %d\n\n", (int) getppid());
    }

    if (pid == 0) {
        printf("Je suis le fils\n");
        printf("Mon pid est : %d\n", (int) getpid());
        printf("Mon ppid est : %d\n\n", (int) getppid());

        sleep(1);
        printf("Je suis toujours le fils\n");
        printf("Mon pid est : %d\n", (int) getpid());
        printf("Mon ppid est : %d\n\n", (int) getppid());
    }

	return EXIT_SUCCESS;
}
