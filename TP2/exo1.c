#include <unistd.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>

pid_t pid;               // pour récupérer le pid du nouveau processus 

int main(int argc, char *argv[]) {
    int filedesc_r, filedesc_w;
    char buf[10];
    int i;

    filedesc_r = open("readfile.txt", O_RDONLY);
    if (filedesc_r == -1) {
        perror("Impossible d'ouvrir le fichier en lecture");
        return EXIT_FAILURE;
    }
    filedesc_w = open("writefile.txt", O_WRONLY | O_APPEND);
    if (filedesc_r == -1) {
        perror("Impossible d'ouvrir le fichier en écriture");
        return EXIT_FAILURE;
    }

    read(filedesc_r, buf, sizeof(char)*10);
    write(filedesc_w, buf, sizeof(char)*10);
    /*for (i = 0; i < 10; i++) {
        read(filedesc_r, buf, sizeof(char));
        write(filedesc_w, buf, sizeof(char));
    }*/

    pid = fork();
    if (pid == 0) {
        while (read(filedesc_r, buf, sizeof(char)) != 0) {
            write(filedesc_w, buf, sizeof(char));
        }
    }
    

    close(filedesc_r);
    close(filedesc_w);
    

    return EXIT_SUCCESS;
}