#include <unistd.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <fcntl.h>

pid_t pid;               // pour récupérer le pid du nouveau processus 

int main(int argc, char *argv[]) {
    printf("Hello world !");

    pid = fork();

    if (pid == 0) {
        printf("\n");
    }
    

    return EXIT_SUCCESS;
}