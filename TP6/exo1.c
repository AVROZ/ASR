#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>

#define R 0
#define W 1

void action (int sig) {
	printf("Je capture le signal\n");
}

int main() {
	int fdp[2], // père vers le fils
		fdf[2]; // fils vers père
	pid_t pid;
	
	char message[100]; // pour récupérer un message
	char *phrasef = "message envoyé au père par le fils";
	char *phrasep = "message envoyé au fils par le père";

	if ((pipe(fdp) != 0) || (pipe(fdf) != 0)) {
		perror("Pipe pas possible");
		exit(1);
	}
	signal( SIGPIPE, &action);
	
	if ((pid = fork()) == -1) {
		perror("Fork pas possible");
		exit(2);
	}
	
	
	if (pid == 0) {
		
		close(fdf[R]); //
		close(fdp[W]); //
		
		if (write( fdf[W], phrasef, strlen(phrasef)+1) > 0) {
			sleep(4);
			int n = read(fdp[R], message, 100); 
			// Dans ce cas le signal SIGPIPE est recu => processus tué
			printf("Valeur de n : %d\n", n);
			if (n > 0) {
				printf("J'ai lu : %s\n", message);
			} else {
				perror("Lecture pas bon"); 
				exit(2);
			}
			close(fdf[W]);
			exit(0);
		} else {
			perror("Write pas bon"); exit(2);
		}
	}
	
	close(fdp[R]);
	close(fdf[W]);
	
	write (fdp[W], phrasep, strlen(phrasep)+1);
	sleep(2);
	read(fdf[R], message, 100);
	printf("J'ai lu : %s\n", message);
	// wait(NULL);
	close(fdp[W]);
	

return 0;
}
