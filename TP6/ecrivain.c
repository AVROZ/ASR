#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

int main (void) {
	int fd;
	char message[26];
	
	sprintf(message, "bonjour du writer [%d]\n", getpid());
	
	fd = open("my_tube", O_WRONLY);
	printf("Ici writer [%d]\n", getpid());
	if (fd != -1) {
		write(fd, message, strlen(message));
	}
	else 
		printf("Désolé, je trouve pas le tube\n");
	
	close(fd);
	return 0;
}