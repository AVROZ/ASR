#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main (void) {
	int fd, n;
	char input;
	
	fd = open("my_tube", O_RDONLY);
	printf("Ici reader [%d]\n", getpid());
	if (fd != -1) {
		printf("Recu par le reader : \n");
		while ((n = read(fd, &input, 1)) > 0) {
			printf("%c", input);
		}
		printf("Le reader a fini sa mission\n");
	}
	else 
		printf("Désolé, je trouve pas le tube\n");
	
	close(fd);
	return 0;
}