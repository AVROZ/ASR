#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>
#include <stdlib.h>

#define MB 20

#define DELTAC 2000
#define DELTAP 3000
#define MIN_B 1
#define MAX_B 10
#define RAND(a,b) ((rand() % (b - a + 1)) + a )

int bonbons = 0;
int deltac = DELTAC;
int deltap = DELTAP;


void disp_candy()
{
	printf("Vitrine : %3d [ ", bonbons);
	for (int i = 0; i < MB; ++i) {
		if (i<bonbons) {
			printf("*");
		} else printf("-");
	}
	printf(" ]\n");
}

void * producteur()
{
	while(1){
		usleep(deltap + rand()%(deltap/10));
		int rd = RAND(MIN_B, MAX_B);
		bonbons += rd;
		printf("producteur (%5hu)   ->  %02d : ", pthread_self(),rd);
		disp_candy();
	}
}

void * consommateur()
{
	while(1){
		usleep(deltac + rand()%(deltac/10));
		int rd = RAND(MIN_B, MAX_B);
		bonbons -= rd;
		printf("consommateur (%5hu) ->  %02d : ", pthread_self(),rd);
		disp_candy();
	}
}

int main(int argc, char * argv[])
{
	int nb_cons,
		nb_prod;

	if (argc < 6){
		printf("usage %s <bonbons> <conso> <prod> <deltap> <deltac>\n",argv[0]);
		exit(1);
	}
	srand(time(NULL));

	bonbons = strtol(argv[1],NULL,0); 

	nb_cons = strtol(argv[3], NULL, 0);
	nb_prod = strtol(argv[2], NULL, 0);

	deltap = strtol(argv[4], NULL, 0);
	deltac = strtol(argv[5], NULL, 0);


	pthread_t * prods = malloc(nb_prod * sizeof(pthread_t));
	pthread_t * cons = malloc(nb_cons * sizeof(pthread_t));

	for (int i = 0; i < nb_prod; ++i) {
		pthread_create(prods+i, NULL, producteur, NULL);//(void *) i);
	}

	for (int i = 0; i < nb_cons; ++i) {
		pthread_create(cons+i, NULL, consommateur, NULL);//(void *) i);
	}

	for (int i = 0; i < nb_cons; ++i) 
		pthread_join(cons[i],NULL);

	for (int i = 0; i < nb_prod; ++i) 
		pthread_join(prods[i],NULL);


	return EXIT_SUCCESS;
}
