## Sockets, multiplexage

#### Ex1 **Révision réseau**
Un serveur tcp tourne sur dwarves.iut-fbleau.fr (port d'écoute : `51234`). Le serveur
envoie a un client connecté un fichier correspondant au  login transmis préalablement
par le client. Vous avez chacun un fichier différent à récupérer.  

Le protocle est le suivant :   
   ```
   Clt                           Srv
        \__>>_______________>>___
                  connect        \__


        \__>>______________>>_____
            sends login           \_
                                verifies login
                                searches for the file 
                                if found then
         __<<______________<<_____/
        /      sends file
   ```   

Appel :   

   ```
	denis@portabledenis:~/$ ./a.out dwarves.iut-fbleau.fr  51234 denis foo.dat
   ```   
Écrire un client qui vous permettent de récupérer le fichier corresponant à votre login. 

#### Ex2
Producteurs/Consommateurs.   
Le but est de programmer, avec les threads, le modèle "Producteur-Consammateur" déjà vu en cours et
en TP, en utilisant les sockets UNIX comme moyen de communication.                                                                                                                 

La ressource commune, une buffer circulaire ("FIFO buffer") sera créée et contrôlée par
un programme serveur  dont les deux types de clients seront les producteurs (un ou plusieurs) et
les consommateurs (un ou plusieurs). La ressource vit tant que le programme serveur tourne.
 
Pour servir les clients, le programme serveur (appelons le `prod-cons-srv`) ouvre deux sockets :
 - `psock`, qui écoute et accepte les requêtes des producteurs,
 - et `csock` qui écoute et accepte les requêtes des consommateurs.

Pour chaque requête acceptée, un nouveau thread est créé par le serveur pour la traiter :

 ```
  master thread---+------------------------+
				  |                        |
		 srv prod part thread       srv cons part thread
			  socket()                  socket()
		   addr prep psock          addr prep csock
			  bind()                    bind()
			  listen()                 listen()
				  |                        |
		   +----->|                 +----->|
		   |   accept               |   accept 
		   \_ _ /   \               \_ _ /   \ 
					|                        |
				 pservice                 cservice
					|                        | 
			+----->recv(how many)          recv(how many)<--+
			|       |                        |              |
			|       |\fin when 0             |\fin when 0   |
			|       | \                      | \            |
			|       |  X                     |  X           |
			| send(actual state)       send(actual state)   |   
			|       |                        |              |
			| put when possible         take when possible  |
			|       |                        |              |
			|    send(report)             send(report)      |
			\_ _ _ /                          \_____________/

```

Ecrivez :
 - `prod-cons-srv.c` qui implante le serveur.
 - `prod-clt.c` qui implante des clients producteurs.
 - `cons-clt.c` qui implante des clients consommateurs.

Chaque client peut consommer ou produire plusieurs unités de ressources gérées par le serveur. C'est le serveur
qui met en place les mécanismes de synchronisations nécessaires (mutex et conditions). 


Remarque : les sockets de famille `AF_UNIX` permet de faire communiquer des processus/threads localement. Le format d'adresse est le
suivant :

```c
#define UNIX_PATH_MAX    108

struct sockaddr_un {
    sa_family_t sun_family;               /* AF_UNIX */
    char        sun_path[UNIX_PATH_MAX];  /* chemin accès */
};
```

On utilise comme adresse un nom de fichier.

#### Ex3 **Client/Serveur de chat**

La fonction `select` (il  existe d'autres fonctions: `poll`, `epoll`) permet de
scruter un ensemble de descripteurs. Elle utilise le type `fd_set`
(bitmap). Le numéro du bit (1024 en tout) correspond à la valeur du descripteur. Ce
type est manipulable avec les fonctions dédiées suivantes :

```c 
void FD_CLR(int fd, fd_set *set);
int  FD_ISSET(int fd, fd_set *set);
void FD_SET(int fd, fd_set *set);
void FD_ZERO(fd_set *set);
```

La fonction `select` permet de se bloquer jusqu'à ce qu'au moins un
descripteur des trois ensembles (lecteure, écriture, exception) soit
prêts pour une lecture ou une écriture (suivant l'ensemble) qui sera
donc non bloquante.

```c
int select(int nfds, fd_set *readfds, fd_set *writefds,
		fd_set *exceptfds, struct timeval *timeout);
```

Seuls les descripteurs `[0,nfds-1]` sont scrutés. À son retour, la
fonction select ne "garde" dans les ensemble passés en argument que
les descripteurs prêts.

#### Serveur
Le serveur se contente de renvoyer les messages qu'il
reçoit aux clients connectés.

 1  Quels sont les descripteurs à scruter ?

 2.  Écrire le serveur qui prend sur la ligne de commande le port d'écoute, 
   en complétant le source joint en annexe.

 3. Testez le client `ncat`, puis avec le votre.

 4. Rendre les sockets non bloquantes pour éviter un blocage sur une
    écriture.

#### Client
Il ne fait que lire sur l'entrée standard des messages
qu'il envoie au serveur, et afficher sur sa sortie standard ce qu'il
reçoit du serveur.

 1.  Quels sont pour le client les descripteurs à surveiller ?

 2.  Écrire un client en mode connecté qui prend sur la ligne de
     commande l'adresse et le port du serveur.

 3.  Testez votre client avec le serveur ncat (il possède une option
     `–chat`).

 4. Améliorez l'interface de votre client en utilisant le source (imparfait)
	[suivant](src) qui utilise la bibliothèque `ncurses`.

	![tchat](img/tchat.png)

	Le nom de l'utilisateur est passé sur la ligne de commande au lancement du client, et n'est pas géré 
	par le serveur. Vous pouvez évidemment rajouter une gestion des utilisateurs de/connectés sur le serveur.


