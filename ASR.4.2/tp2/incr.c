#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc,char * argv[])
{
	int shmid,
        semid,
	    i,
	    *count;
	
	char buf[256];
	key_t k = ftok("/tmp",1);
	assert(k!=-1);
	key_t k2 = ftok("/tmp",2);
	assert(k2!=-1);

	shmid=shmget(k, sizeof(int), IPC_CREAT|0666);
	assert(shmid >= 0);

	count = (int*)shmat(shmid,NULL,0);
	assert(count != (void*)-1);

    i = atoi(argv[1]);

    semid = semget(k2, 1, IPC_CREAT|0600);
	assert(semid >= 0);

	struct sembuf p = {0,-1,0};	//{{0,-1,0},{0,1,0}}
	struct sembuf v = {0,1,0};

	// printf("Trying to lock...\n");
    // if (semop(semid, p, 1) == -1) { 
    //     perror("semop");
    //     exit(1); 
    // }
	// assert(semop(semid, p, 1) >= 0);
    // printf("Locked.\n");

	semop(semid, &p, 1);
	printf("Test\n");

	for (i=1;i<=100000;i++){
		*count = *count + 1;
	}

	semop(semid, &v, 1);
	
	// assert(semop(semid, v, 1) >= 0);
    printf("Unlocked.\n");
}
