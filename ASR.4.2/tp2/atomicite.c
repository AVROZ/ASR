#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>

union semun {                   /* Used in calls to semctl() */
	int                 val;
	struct semid_ds *   buf;
	unsigned short *    array;
#if defined(__linux__)
	struct seminfo *    __buf;
#endif
};

int main(int argc,char * argv[])
{
	int shmid,
        semid,
	    i,
	    *count;
	
	union semun arg;

	char buf[256];
	key_t k = ftok("/tmp",1);
	assert(k!=-1);
	key_t k2 = ftok("/tmp",2);
	assert(k2!=-1);

	shmid=shmget(k, sizeof(int), IPC_CREAT|0666);
	assert(shmid >= 0);

	count = (int*)shmat(shmid,NULL,0);
	assert(count != (void*)-1);

    semid = semget(k2, 1, IPC_CREAT|IPC_EXCL|0600);
	arg.val = 1;
	//assert(semctl(semid, 0, SETVAL, 1) >= 0);
	if (semctl(semid, 0, SETVAL, arg) == -1) {
		perror("semctl");
		exit(1);
	}

    // op.sem_num = 0; //Numéro de notre sémaphore: le premier et le seul
    // op.sem_op = -1; //Pour un P() on décrémente
    // op.sem_flg = 0; //On s'en fout

    // struct sembuf op;
	struct sembuf p = {0,-1,0};
	struct sembuf v = {0,1,0};

	*count = 0;

	for (i=1;i<=5;i++){
		pid_t p = fork();
		assert( p != -1);

		if (p==0) {
			snprintf(buf,sizeof(buf),"%d",i);
			execl("./incr","./incr",buf,NULL);
			assert(0);
		}
	}

	for (i=1;i<=5;i++) wait(NULL);

	printf("count = %d\n",*count);
	assert(semctl(semid, 0, IPC_RMID, 0) >= 0);
	assert(shmctl(shmid,IPC_RMID,0) >=0);
}
