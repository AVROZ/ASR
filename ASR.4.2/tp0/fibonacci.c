#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
long int fibo (long int n)
{
	return (n<=1)?n:fibo(n-1)+fibo(n-2);
}



int main(int argc, char *argv[])
{
	long int n;
	assert(argc > 1);
	pid_t pid1 = fork();
	pid_t pid2 = fork();
	long int result;
	n=strtol(argv[1],NULL,0);

	if (pid1 == 0) {	// Père
		
	} else {	//Fils 1
		long int fibo1 = fibo(n-2);

		pid2 = fork();	//Fils 2
		if (pid2 == 0) {
			long int fibo2 = fibo(n-1);
		}
	}

	//printf("fibo(%ld) = %ld\n",n,fibo(n));
	printf("fibo(%ld) = %ld\n",n,result);
	return 0;
}
