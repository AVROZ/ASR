#ifndef _ETAT_H
#define _ETAT_H



typedef enum {  PENSE, AFAIM, MANGE } etat ;
//char * etat_nom [] = {"est a table", "pense","a faim","mange"};
typedef struct etat_philosophe {
	int no  ; // le philosophe
	etat ep; // son etat
} etat_philosophe ;

int ecrire_etat(etat_philosophe *e);
int lire_etat(etat_philosophe *e);

#endif
