#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "etat.h"
#include "helpers.h"
#define DELTA 100000
int semid=-1;

void arret(int signum){
	fprintf(stderr,"ARRET\n");
//	assert(semctl(semid,IPC_RMID,0)>=0);
	kill(0,SIGTERM);
}

void run_philo(int no,int nb){
	etat_philosophe e = { no, PENSE} ;
	int left = no ; 
	int right = ( no == nb -1) ? 0 : no + 1;


	srand(getpid());

	// on attend que tout le monde soit à
	// table 
	//

	usleep(rand()%DELTA);
	ecrire_etat(&e);


	while(1){

		usleep(rand()%DELTA);
		e.ep = AFAIM;
		ecrire_etat(&e);

		// PRENDRE FOURCHETTE

		e.ep= MANGE;
		ecrire_etat(&e);

		usleep(rand()%DELTA);

		e.ep = PENSE;
		ecrire_etat(&e);

		// REPOSER FOURCHETTE
	}
}


int main(int argc,char * argv[])
{
	int nb_philo,
		i;
	unsigned short  * val; 
	assert(argc == 2);	
	nb_philo = strtol(argv[1],NULL,0);


	for (i=0;i<nb_philo;i++){
		pid_t p = fork();
		assert( p != -1);

		if (p==0) {
			run_philo(i,nb_philo);
		}
	}

	assert(set_signal_handler(SIGINT,arret)==0);
	for (i=0;i<nb_philo;i++) 
		wait(NULL);
}
