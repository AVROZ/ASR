#include "etat.h"
#include <unistd.h>


int ecrire_etat(etat_philosophe *e){
	 return write(1,e,sizeof(etat_philosophe)) == sizeof(etat_philosophe);
}

int lire_etat(etat_philosophe * e){
	 return read(0,e,sizeof(etat_philosophe)) == sizeof(etat_philosophe);
}
