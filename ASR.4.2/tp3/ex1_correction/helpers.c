#include "helpers.h"
#include <signal.h>
#include <stdlib.h>
int set_signal_handler(int signo, void (*handler)(int)) 
{
	struct sigaction sa;
	sa.sa_handler = handler;    // call `handler` on signal
	sigemptyset(&sa.sa_mask);   // don't block other signals in handler
	sa.sa_flags = 0;            //  don't restart system calls
	return sigaction(signo, &sa, NULL);
}


