#include "etat.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define couleur(param) fprintf(stdout,"\033[%sm",param)

#define NOIR  "30"
#define ROUGE "31"
#define VERT  "32"
#define JAUNE "33"
#define BLEU  "34"
#define CYAN  "36"
#define BLANC "37"
#define REINIT "0"


char * etat_nom [] = {"pense","a faim","mange"};
void afficher_etat(etat etats[],int nb_etat,int nb_philo)
{
	int i,j;

	for (i=0;i<nb_etat-1;i++){

		printf(" %s : ",etat_nom[i]);
		for (j=0;j<nb_philo;j++){
			if (etats[j] == i) 
				printf("%d.",j);
			else printf("..");
		}
	}
	printf(" %s : ",etat_nom[2]);
	for (j=0;j<nb_philo;j++){

		if (etats[j] == 2) {
			int left = (j>0)?j-1:nb_philo-1;
			int right = (j<nb_philo-1)?j+1:0;
			if (etats[left] == 2 || etats[right] == 2)
				couleur(ROUGE);
			printf("%d.",j);
			couleur(REINIT);

		}
		else printf("..");


	}

		printf("\n");
}

int main (int argc , char * argv[])
{

	int nb_philo,
		nb_etat = 3,
		i;
	etat  * etats;
	etat_philosophe e;
	nb_philo = strtol(argv[1], NULL,0);

	etats = (etat*)calloc(nb_philo,sizeof(etat));
	for(i=0;i<nb_philo;i++) 
		etats[i] = -1;

	afficher_etat(etats,nb_etat,nb_philo);
	while(1){
		if (lire_etat(&e) == 0) 
			exit(0);
		etats[e.no] = e.ep;
		afficher_etat(etats,nb_etat,nb_philo);
	}
}
