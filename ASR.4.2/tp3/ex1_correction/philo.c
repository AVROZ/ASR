#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "etat.h"
#include "helpers.h"
#define DELTA 100000
int semid = -1;
int shmid = -1;
int * count = NULL;
int nbp = -1;
void arret(int signum){
	fprintf(stderr,"ARRET\n");
	for(int i=0;i<nbp;i++)
		fprintf(stderr,"philo %2d : %3d\n",i,count[i]);
	assert(semctl(semid,IPC_RMID,0)>=0);
	assert(shmctl(shmid,IPC_RMID,0)>=0);
	kill(0,SIGTERM);
}

void run_philo(int no,int nb){
	etat_philosophe e = { no, PENSE} ;
	int left = no ; 
	int right = ( no == nb -1) ? 0 : no + 1;
	struct sembuf rendez_vous [] = {{ nb,-1,0} , {nb,0,0}};
	struct sembuf prendre_fourchette [] = {{left,-1,0},{right,-1,0}};
	struct sembuf rendre_fourchette [] = {{left,+1,0},{right,+1,0}};
	srand(getpid());

	// on attend que tout le monde soit à
	// table 
	//
	
	usleep(rand()%DELTA);
	ecrire_etat(&e);
	semop(semid,rendez_vous,1);
	semop(semid,rendez_vous+1,1);

	while(1){

		usleep(rand()%DELTA);
		e.ep = AFAIM;
		ecrire_etat(&e);
		semop(semid,prendre_fourchette,2);
		// PRENDRE FOURCHETTE

		e.ep= MANGE;
		ecrire_etat(&e);
		count[no] ++;
		usleep(rand()%DELTA);

		e.ep = PENSE;
		ecrire_etat(&e);

		semop(semid,rendre_fourchette,2);
		// REPOSER FOURCHETTE
	}
}


int main(int argc,char * argv[])
{
	int nb_philo,
		i;
	unsigned short  * val; 
	assert(argc == 2);	
	nb_philo = strtol(argv[1],NULL,0);
	nbp = nb_philo;
	semid = semget (IPC_PRIVATE, nb_philo + 1, 0600);
	assert(semid >=0);

	val = (unsigned short *) calloc(nb_philo + 1,sizeof(unsigned short));
	for(i=0;i<nb_philo;i++)
		val[i] = 1;
	val[nb_philo] = nb_philo;
	assert(semctl(semid,0,SETALL,val)>=0);

	shmid = shmget(IPC_PRIVATE,nb_philo * sizeof(int),0600);
	assert(shmid >=0);
	count = shmat(shmid,NULL,0);
	for (i=0;i<nb_philo;i++)
		count[i] = 0;

	for (i=0;i<nb_philo;i++){
		pid_t p = fork();
		assert( p != -1);

		if (p==0) {
			run_philo(i,nb_philo);
		}
	}

	assert(set_signal_handler(SIGINT,arret)==0);
	for (i=0;i<nb_philo;i++) 
		wait(NULL);
}
