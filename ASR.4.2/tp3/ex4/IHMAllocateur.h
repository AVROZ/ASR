#ifndef _IHMAllocateur_H
#define _IHMAllocateur_H


#define bufL 10 
#define bufH 4 

typedef enum { libre,plein } state;

typedef struct IHMProcessus {
	int no;
	WINDOW * w; /* fenetre principale du processus */
	WINDOW ** tampon; /* case du tampon */
	int d;
	state * t;
	char label[20];
} IHMProcessus;

typedef struct IHMTampon {
	int libre;
	WINDOW * w;
	WINDOW ** tampon;
	state * t;

} IHMTampon;

typedef struct IHM {
	int nbProc;
	int capacite;
	IHMTampon buffer;
	IHMProcessus  * processus;
	WINDOW * m;

} IHM;


#endif
