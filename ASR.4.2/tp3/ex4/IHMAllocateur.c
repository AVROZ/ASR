#include<curses.h>
#include<string.h>
#include<math.h>
#include<stdlib.h>
#include<unistd.h>
#include "IHMAllocateur.h"
#include "message.h"

void repaintTampon(IHM * ihm)
{
	int j;
	int row,col;
	char buf[255];
	int nbRess = ihm->capacite;
	//wclear(ihm->buffer.w);
	
	werase(ihm->buffer.w);	
	wattron(ihm->buffer.w,COLOR_PAIR(9));
	getmaxyx(ihm->buffer.w,row,col);
	sprintf(buf,"%d libres",ihm->buffer.libre);
	mvwaddstr(ihm->buffer.w, row-2, bufL/2-strlen(buf)/2, buf);
	//	mvwaddstr(ihm->buffer.w, row-2, 0, buf);

	for(j=0;j<nbRess;j++){
		if (ihm->buffer.t[j] != libre){
			wbkgd(ihm->buffer.tampon[j],COLOR_PAIR(9));
		}
		else{
			werase(ihm->buffer.tampon[j]);
			wbkgd(ihm->buffer.tampon[j],COLOR_PAIR(1)|'#');
		}
		box(ihm->buffer.tampon[j],0,0);
	}
	wrefresh(ihm->buffer.w);	
//	werase(ihm->buffer.w);	
}


void repaintProcessus(IHM * ihm,int i)
{
	int j;
	int row,col;
	char buf[255];
	int nbRess = ihm->capacite;
	wclear(ihm->processus[i].w);
	if (ihm->processus[i].d != 0){
	wattron(ihm->processus[i].w,COLOR_PAIR(1)|A_BOLD);

			werase(ihm->processus[i].w);
//		wbkgd(ihm->processus[i].w,COLOR_PAIR(3)|' ');
		sprintf(buf,"->%d",ihm->processus[i].d);
		mvwaddstr(ihm->processus[i].w, 1,  1+bufL/2-strlen(buf)/2, buf);
	}else{
		wattroff(ihm->processus[i].w,A_BOLD);
		wattron(ihm->processus[i].w,COLOR_PAIR(9)|A_NORMAL);

	}
	getmaxyx(ihm->processus[i].w,row,col);
	mvwaddstr(ihm->processus[i].w, row-2, 1+bufL/2-strlen(ihm->processus[i].label)/2, ihm->processus[i].label);

	box(ihm->processus[i].w, 0, 0);
	for(j=0;j<nbRess;j++){
		if (ihm->processus[i].t[j] == libre){
			//			werase(ihm->processus[i].tampon[j]);
			wbkgd(ihm->processus[i].tampon[j],COLOR_PAIR(9));
		}
		else{

			werase(ihm->processus[i].tampon[j]);
			wbkgd(ihm->processus[i].tampon[j],COLOR_PAIR(1)|'#');
		}
		box(ihm->processus[i].tampon[j],0,0);
	}
	wrefresh(ihm->processus[i].w);	
}

//void repaintAllProcessus(IHMProcessus processus[nbProc]){
void repaintAllProcessus(IHM * ihm)
{

	int i;
	int nbProc = ihm->nbProc;
	for(i=0;i<nbProc;i++) repaintProcessus(ihm,i);
}

//void initProcessus(IHMProcessus processus[nbProc]){
void initProcessus(IHM * ihm)
{
	int i,j;
	int row,col=0;
	int nbProc = ihm->nbProc;
	int nbRess = ihm->capacite;
	ihm->processus = (IHMProcessus*)malloc(nbProc * sizeof(IHMProcessus));
	for(i=0;i<nbProc;i++){

		ihm->processus[i].w = newwin(bufH*(nbRess+2),bufL+2,(LINES/bufH-nbRess-4)*bufH,15*i+50);

		wattron(ihm->processus[i].w,COLOR_PAIR(0));
		ihm->processus[i].d = 0;
		sprintf(ihm->processus[i].label,"P%d",i);
		getmaxyx(ihm->processus[i].w,row,col);
		mvwaddstr(ihm->processus[i].w, row-2, bufL/2-strlen(ihm->processus[i].label)/2, ihm->processus[i].label);
		if (ihm->processus[i].d) wattron(ihm->processus[i].w,COLOR_PAIR(1));

		box(ihm->processus[i].w, 0, 0);
		ihm->processus[i].tampon = (WINDOW **) malloc(ihm->capacite * sizeof(WINDOW*));
		ihm->processus[i].t = (state *) malloc(ihm->capacite * sizeof(state));
		for(j=0;j<nbRess;j++){
			ihm->processus[i].tampon[j]=derwin(ihm->processus[i].w,bufH,bufL,bufH*(j+1),1);
			werase(ihm->processus[i].tampon[j]);
			wbkgd(ihm->processus[i].tampon[j],COLOR_PAIR(9));
			box(ihm->processus[i].tampon[j], 0, 0);
			ihm->processus[i].t[j]=libre;
		}
	}
}


//void initTampon(IHMTampon  * buffer){
void initTampon(IHM * ihm)
{
	int i,j;
	int row,col=0;
	char buf[256];
	int nbRess = ihm->capacite;
	ihm->buffer.w = newwin(bufH*(nbRess+2),bufL+2,(LINES/bufH-nbRess-4)*bufH,10);

	wattron(ihm->buffer.w,COLOR_PAIR(0));
	sprintf(buf,"%d libres",nbRess);
	getmaxyx(ihm->buffer.w,row,col);
	mvwaddstr(ihm->buffer.w, row-2, 1+bufL/2-strlen(buf)/2, buf);
	ihm->buffer.tampon = (WINDOW **) malloc(sizeof(WINDOW*)*ihm->capacite);
	ihm->buffer.t = (state *)malloc(sizeof(state)*nbRess);
	for(j=0;j<nbRess;j++){
		ihm->buffer.tampon[j]=derwin(ihm->buffer.w,bufH,bufL,bufH*(j+1),1);
		wbkgd(ihm->buffer.tampon[j],COLOR_PAIR(0));
		box(ihm->buffer.tampon[j], 0, 0);
		ihm->buffer.t[j]=libre;
	}
	ihm->buffer.libre = nbRess;
}


void messageIHM(IHM * ihm, char * m)
{

	int col,row;
	getmaxyx(ihm->m,row,col);
	//wclear(ihm->m);
	werase(ihm->m);
	box(ihm->m,0,0);
	mvwaddstr(ihm->m, row/2, col/2-strlen(m)/2, m);
	wrefresh(ihm->m);	
}


void IHMProcessAction(message m,IHM *h)
{
	char buf[255];
	int i,j;
	int nbRess = h->capacite;

	switch(m.a){
		case demande :
			h->processus[m.no].d=m.ressources;
			sprintf(buf,"%d demande %d",m.no,m.ressources);
			messageIHM(h,buf);
			repaintProcessus(h,m.no);//h->processus[m.no]);
			break;
		case obtient :
			h->processus[m.no].d=0;
			sprintf(buf,"%d obtient %d",m.no,m.ressources);
			h->buffer.libre = h->buffer.libre - m.ressources;
			for(i=nbRess-1,j=0;j<m.ressources;i--,j++) h->processus[m.no].t[i] = plein;
			for(i=nbRess-1,j=0;i>=0;i--,j++) if (j<h->buffer.libre) h->buffer.t[i] = libre;else h->buffer.t[i] = plein;
			messageIHM(h,buf);
			repaintProcessus(h,m.no);//->processus[m.no]);
			repaintTampon(h);
			break;
		case rend :
			h->processus[m.no].d=0;
			sprintf(buf,"%d rend %d",m.no,m.ressources);

			h->buffer.libre = h->buffer.libre + m.ressources;
			for(i=nbRess-1,j=0;j<m.ressources;i--,j++) h->processus[m.no].t[i] = libre;

			for(i=nbRess-1,j=0;i>=0;i--,j++) if (j<h->buffer.libre) h->buffer.t[i] = libre;else h->buffer.t[i] = plein;
			messageIHM(h,buf);
			repaintProcessus(h,m.no);//->processus[m.no]);
			repaintTampon(h);//->buffer);
			break;
	}
}

void initIHM(IHM *ihm,int nbProc,int capacite)
{
	ihm->capacite = capacite;
	ihm->nbProc = nbProc;
	ihm->m = newwin(bufH,COLS-4,LINES-bufH,2);
//	ihm->m = newwin(bufH,COLS-4,2,2);
	box(ihm->m,0,0);
	wrefresh(ihm->m);	
	initProcessus(ihm);
	initTampon(ihm);
}

int main(int argc,char *argv[])
{
	IHM ihm;
	message m;
	int nbProc=5;
	int capacite=5;
	if (argc == 3){
		nbProc=strtol(argv[1],NULL,0);
		capacite=strtol(argv[2],NULL,0);
	}
	initscr();          /* Start curses mode        */
	cbreak(); 
	curs_set(FALSE);
	keypad(stdscr, TRUE);       /* I need that nifty F1     */
	noecho();

	if (has_colors())
	{
		start_color();
		use_default_colors();
		init_pair(1, COLOR_RED,     -1);
		init_pair(2, COLOR_GREEN,   -1);
		init_pair(3, COLOR_YELLOW,  COLOR_BLACK);
		/*	init_pair(4, COLOR_BLUE,    COLOR_BLACK);
			init_pair(5, COLOR_CYAN,    COLOR_BLACK);
			init_pair(6, COLOR_MAGENTA, COLOR_BLACK);
			init_pair(7, COLOR_BLACK,   COLOR_WHITE);
			init_pair(8, COLOR_GREEN,   COLOR_RED);
		//init_pair(9, COLOR_WHITE,   COLOR_BLACK);*/
		init_pair(9, -1,   -1);
	}
	initIHM(&ihm,nbProc,capacite);
	repaintAllProcessus(&ihm);
	repaintTampon(&ihm);

	while(1){
		m=lire_message();
		IHMProcessAction(m,&ihm);
		usleep(1000000);
	}
	//getchar();
	endwin();
}

