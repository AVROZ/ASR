#include "message.h"
#include <unistd.h>


void ecrire_message(message m)
{

	write(OUT,&m,sizeof(message));
}

message lire_message()
{
	message m;
	read(IN,&m,sizeof(message));
	return m;

}
