#include "allocateur.h"
#include "processus.h"
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include<stdlib.h>
#include<signal.h>
#include<time.h>
#include <assert.h>
#include <stdio.h>

void handler_fin(int n)
{
	delete_allocateur();
	exit(0);
}

int main(int argc,char *argv[])
{
	struct sigaction sa={0};

	int nb_proc,      // nombre de processus
		nb_rc,		  // nomre de ressources
		i;

	if (argc != 3){
		printf("usage : %s nb_processeus nb_ressources\n",argv[0]);
		exit(1);
	}

	nb_proc = (int)strtol(argv[1],NULL,0);
	nb_rc = (int)strtol(argv[2],NULL,0);

	init_allocateur(nb_rc);

	sigemptyset(&(sa.sa_mask));
	sa.sa_handler = handler_fin;

	assert(sigaction(SIGTERM,&sa,NULL) == 0);
	assert(sigaction(SIGINT,&sa,NULL) == 0);


	for (i=0;i<nb_proc;i++){

		if (fork() == 0){
			srand(time(NULL) ^ (getpid()<<16));
			run_processus(i);
		}
	}


	for (i=0;i<nb_proc;i++) 
		wait(NULL);
}
