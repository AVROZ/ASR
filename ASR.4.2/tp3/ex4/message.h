#ifndef _MESSAGE_H
#define _MESSAGE_H
#define IN 0
#define OUT 1

typedef enum { demande, obtient, rend } action;
typedef struct message 
{
	int no ; // numero processus
	action a; // quoi 
	int ressources;
} message ;

message lire_message();
void ecrire_message(message);
#endif


