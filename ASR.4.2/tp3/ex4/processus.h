#ifndef _PROCESSUS_H
#define _PROCESSUS_H

#define MinDelayUtilise  800
#define MaxDelayUtilise  2000
#define MinDelayPense  20*MinDelayUtilise
#define MaxDelayPense 20*MaxDelayUtilise


void  run_processus(int no);
#endif

