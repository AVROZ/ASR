#include <sys/sem.h>
#include "allocateur.h"
#include <stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include "message.h"

static int _semid;
static int _ressources;

int capacite()
{
return _ressources;
}

void demander(int no,int n)
{
	message m ;
	m.no = no;
	m.a=demande;
	m.ressources=n;

	ecrire_message(m);
	//
	// code
	//
	m.a=obtient;
	ecrire_message(m);
}

void rendre(int no,int n)
{
	message m ;

	m.no = no;
	m.a=rend;
	m.ressources=n;
	//
	// code
	//
	ecrire_message(m);
}

void init_allocateur(int rc)
{

	//
	// code
	// 
	_ressources = rc;
}

void delete_allocateur()
{
	//
	// code
	//
}

