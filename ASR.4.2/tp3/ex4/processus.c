#include<stdlib.h>
#include <unistd.h>
#include<stdio.h>
#include "message.h"
#include "processus.h"
#include "allocateur.h"

int _choisir_demande(int i){
	return (1+rand()%capacite());
}


void _processus_sleep(int i,int bi,int bs){
	usleep ((rand()%(bs - bi + 1) + bi)*1000);
}

void  run_processus(int i){
	int dem;
	//_processus_sleep(i,0,MaxDelayPense/2);
	while(1){
		dem = _choisir_demande(i);
		//		demander(i,dem);
		//printf("%d demande %d\n",i,dem);
		demander(i,dem);

		//printf("%d utilise %d\n",i,dem);
		//		utiliser(i,dem);
		_processus_sleep(i,MinDelayUtilise,MaxDelayUtilise);

		rendre(i,dem);

		//printf("%d a rendu %d\n",i,dem);
		//		liberer(dem);
		_processus_sleep(i,MinDelayUtilise,MaxDelayUtilise);
	}
}
