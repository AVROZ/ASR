#include<stdio.h>
#include<stdlib.h>
#include <string.h>

void print_int(void *a) {
    printf("%d ", *(int*) a);
}

void print_string(void *s) {
    printf("%s ", *(char**) s);
}

int cmp_int( const void * first, const void * second ) {
	int firstInt = * (const int *) first;
    int secondInt = * (const int *) second;
    return firstInt - secondInt;
}

int cmp_tring( const void * first, const void * second ) {
	return strcmp(*(char**) first, *(char**) second);
}

void map(void *base,size_t n ,size_t size_elem,void(*f)(void *)){
	int i;
	char *ptr = (char*)base; 
	for(i=0;i<n;i++) 
		f((void*)(ptr+i*size_elem)); 
}

int main(){
	int t1[10]={12,-7,1,-16,3,19,7,1,5,0};
	char * t2[]={"chou","joujou","bijou","genou",
		"caillou","hibou","pou"};

	map(t1,10,sizeof(int),print_int);
	map(t2,7,sizeof(char*),print_string);

	printf("\n");

	qsort(t1,10,sizeof(int),cmp_int);
	qsort(t2,7,sizeof(char * ),cmp_tring);

	map(t1,10,sizeof(int),print_int);
	map(t2,7,sizeof(char*),print_string);
}
