#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>

// struct addrinfo {
//     int ai_flags;
//     int ai_family;
//     int ai_socktype;
//     int ai_protocol;
//     socklen_t ai_addrlen;
//     struct sockaddr *ai_addr;
//     char *ai_canonname;
//     struct addrinfo *ai_next;
// };

int main(int argc, char *argv[]){

    int r;
    int sb;

    struct addrinfo criteria;
    struct addrinfo *res,*resp;
    char host[NI_MAXHOST], service[NI_MAXSERV];

    if (argc < 2){
        printf("Usage: %s <port>\n",argv[0]);
        exit(1);
    }

    memset(&criteria, 0, sizeof(struct addrinfo));
    criteria.ai_family = AF_UNSPEC; /* Allow IPv4 or IPv6 */
    criteria.ai_socktype = 0; //Socket addresses of any type
    //The first in the returned list is set to point to the official name of the host
    criteria.ai_flags = AI_CANONNAME;
    criteria.ai_protocol = 0; /* Any protocol */
    r = getaddrinfo(argv[1], argv[2], &criteria, &res);
    if (r != 0){
        fprintf(stderr, "getaddrinfo fails: %s\n", gai_strerror(r));
        exit(EXIT_FAILURE);
    }

    sb = sock_bind();

    resp = res;
    while (resp != NULL){
        getnameinfo(resp->ai_addr, resp->ai_addrlen, host, sizeof(host), service, NI_MAXSERV, NI_NUMERICHOST);
        //getnameinfo(resp->ai_addr, resp->ai_addrlen, host, NI_MAXHOST, service, NI_MAXSERV, 0);
        printf("%s.%s\n", host, service);
        printf("fam: %d -- socktype: %d -- pro: %d -- addrlen: %d\n", resp->ai_family, resp->ai_socktype, resp->ai_protocol, resp->ai_addrlen);

        resp = resp->ai_next;
    }
    freeaddrinfo(res);

    exit(0);
}