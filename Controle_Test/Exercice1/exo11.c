#include <signal.h> 
#include <stdio.h> 
#include <stdlib.h>
#include <unistd.h> 
#include <sys/wait.h>

void action1 (int signum); /* sur SIGUSR1 */
void action2 (int signum); /* sur SIGINT */
void plus();

int pid;
int count;

int main(int argc, char *argv[]) {
	pid = fork();
	
	if (pid == 0) {
        signal(SIGUSR1, &action1);
        signal(SIGINT, &action2);
        pause();
        pause();
        pause();
	}
	else {
        sleep(2);
        kill(pid, SIGUSR1);
        //printf("Dans le père après l'envoie du signal count = %d\n", count);
        sleep(2);
        kill(pid, SIGUSR1);
        //printf("Dans le père après l'envoie du signal count = %d\n", count);
        sleep(2);
        kill(pid, SIGINT);
        sleep(2);

        //printf("Mon fils à reçu %d signaux\n", count);
        wait(NULL);
    }

    if (pid == 0) {
        printf("count = %d\n", count);
    }
    

	return EXIT_SUCCESS;
}

void action1 (int signum) {
    printf("Je viens de reçevoir un SIGUSR1\n");
	plus();
}

void action2(int signum) {
    printf("Je viens de reçevoir un SIGINT\n");
	plus();
}

void plus() {
    count++;
}