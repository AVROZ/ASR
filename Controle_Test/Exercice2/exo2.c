#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
    int pid;
    int fd1[2], fd2[2];
    int *pidof1, *pidof2;

    pipe(fd1);
    pipe(fd2);
    
    if ((pid = fork()) == 0) {
        // Envoie du pid de ce processus vers le 2ème
        close(fd1[0]);
        *pidof1 = getpid();
        write(fd1[1], pidof1, sizeof(int));

        printf("Je suis le processus 1 et mon pid est %d\n", *pidof1);
        close(fd1[1]);

        // Réception du pid du 2ème processus
        sleep(2);
        close(fd2[1]);
        if (read(fd2[0], pidof2, sizeof(int)) == -1) {
            perror("An error occured with read");
            exit(2);
        }

        printf("Je suis le processus 1 et le pid du 2 est %d\n", *pidof2);
        close(fd2[0]);
    } else {
        if ((pid = fork()) == 0) {
            // Réception du pid du 1er processus
            close(fd1[1]);
            read(fd1[0], pidof1, sizeof(int));

            printf("Je suis le processus 2 et le pid du 1 est %d\n", *pidof1);
            close(fd1[0]);

            // Envoie du pid de ce processus vers le 1er
            sleep(2);
            printf("pid proc 2 = %d\n", getpid());
            close(fd2[0]);
            *pidof2 = getpid();
            if (write(fd2[1], pidof2, sizeof(int)) == -1) {
                perror("An error occured with write");
                exit(2);
            }
            
            printf("Je suis le processus 2 et mon pid est %d\n", *pidof2);
            close(fd2[1]);
        } else {
            wait(NULL);
        }

        wait(NULL);
    }

    
    /*
    if ((pid = fork()) == 0) {
        
    }*/
}